import dis
import inspect


def intord(value):
    if type(value) != int:
        return ord(value)
    return value

def disassemble(func):
    """Disassemble a code object."""
    lasti = -1
    myprint = lambda *args, **kwargs: None
    co = func.__code__
    code = co.co_code
    labels = dis.findlabels(code)
    linestarts = dict(dis.findlinestarts(co))
    n = len(code)
    i = 0
    extended_arg = 0
    free = None
    func_code = []
    while i < n:
        instr = []
        op = intord(code[i])
        if i in linestarts:
            if i > 0:
                myprint()
            myprint("%3d" % linestarts[i], end=' ')
        else:
            myprint('   ', end=' ')

        if i == lasti: myprint('-->', end=' ')
        else: myprint('   ', end=' ')
        if i in labels: myprint('>>', end=' ')
        else: myprint('  ', end=' ')
        myprint(repr(i).rjust(4), end=' ')
        myprint(dis.opname[op].ljust(20), end=' ')
        instr.append(dis.opname[op].lower())
        i = i+1
        if op >= dis.HAVE_ARGUMENT:
            oparg = intord(code[i]) + intord(code[i+1])*256 + extended_arg
            extended_arg = 0
            i = i+2
            if op == dis.EXTENDED_ARG:
                extended_arg = oparg*65536
            myprint(repr(oparg).rjust(5), end=' ')
            if dis.opname[op] == 'CALL_FUNCTION':
                instr.append(oparg)
            if op in dis.hasconst:
                myprint('(' + repr(co.co_consts[oparg]) + ')', end=' ')
                instr.append(co.co_consts[oparg])
            elif op in dis.hasname:
                myprint('(' + co.co_names[oparg] + ')', end=' ')
                instr.append(co.co_names[oparg])
            elif op in dis.hasjrel:
                myprint('(to ' + repr(i + oparg) + ')', end=' ')
                instr.append(i + oparg)
            elif op in dis.haslocal:
                myprint('(' + co.co_varnames[oparg] + ')', end=' ')
                instr.append(co.co_varnames[oparg])
            elif op in dis.hascompare:
                myprint('(' + cmp_op[oparg] + ')', end=' ')
                instr.append(cmp_op[oparg])
            elif op in dis.hasfree:
                if free is None:
                    free = co.co_cellvars + co.co_freevars
                myprint('(' + free[oparg] + ')', end=' ')
                instr.append(free[oparg])
        myprint()
        func_code.append(tuple(instr))
    return tuple(func_code)



class PrologFunctionPrinter:
    def __init__(self, func):
        self._func = func
        self._argspec = inspect.getargspec(func)
        self._stack = []
        self._locals = {}
        self._var_counter = 0

        for arg in self._argspec.args:
            self._locals[arg] = "V_" + arg


    def _nextvar(self):
        varid = self._var_counter
        self._var_counter += 1
        return "T%s" % varid

    def visit(self, code):
        prolog_body = []
        for instr in code:
            func = getattr(self, instr[0])
            prolog_body.extend(func(*instr[1:]))
        
        prolog_args = ", ".join(tuple("V_%s" % a for a in self._argspec.args) + ('ReturnValue', ))
        prolog_code = "py_%s(Module, %s) :-\n" % (self._func.__name__, prolog_args)
        prolog_code += ",\n".join("    %s" % i for i in prolog_body) + ".\n\n"
        return prolog_code

    def load_global(self, name):
        result = self._nextvar()
        self._stack.append(result)
        return ["load_attr(Module, '%s', %s)" % (name, result)] # TODO: This is not correct. The func.__module__ should be used instead.

    def store_global(self, name):
        expr = self._stack.pop()
        return ["store_attr(Module, '%s', %s)" % (name, expr)] # see load_global

    def load_fast(self, varname):
        prolog_name = self._locals[varname]
        self._stack.append(prolog_name)
        return []

    def load_attr(self, attrname):
        target = self._stack.pop()
        result = self._nextvar()
        self._stack.append(result)
        return ["load_attr(%s, '%s', %s)" % (target, attrname, result)]

    def store_attr(self, attrname):
        target = self._stack.pop()
        expr = self._stack.pop()
        return ["store_attr(%s, '%s', %s)" % (target, attrname, expr)]

    def store_fast(self, varname):
        expr = self._stack.pop()
        self._locals[varname] = expr
        return []

    def load_const(self, value):
        prolog_value = "py_%s" % value.__class__.__name__
        self._stack.append(prolog_value)
        return []

    def binary_add(self):
        right = self._stack.pop()
        left = self._stack.pop()
        result = self._nextvar()
        self._stack.append(result)
        return ['binary_add(%s, %s, %s)' % (left, right, result)]

    def binary_subtract(self):
        right = self._stack.pop()
        left = self._stack.pop()
        result = self._nextvar()
        self._stack.append(result)
        return ['binary_subtract(%s, %s, %s)' % (left, right, result)]

    def binary_subscr(self):
        index = self._stack.pop()
        target = self._stack.pop()
        result = self._nextvar()
        self._stack.append(result)
        return ['binary_subscr(%s, %s, %s)' % (target, index, result)]

    def return_value(self):
        expr = self._stack.pop()
        return ['return_value(%s, ReturnValue)' % expr]

    def call_function(self, num_args):
        start = len(self._stack) - num_args
        args = self._stack[start:]
        del self._stack[start:]
        func = self._stack.pop()
        result = self._nextvar()
        self._stack.append(result)
        return ['call_function(Module, %s, [%s], %s)' % (func, ", ".join(args), result)]

def prolog_predicate_for_python_module(func):
    argspec = inspect.getargspec(func)
    code = disassemble(func)
    return PrologFunctionPrinter(func).visit(code)

function_class = prolog_predicate_for_python_module.__class__
def python_value_to_prolog(value):
    cls = value.__class__
    if cls == function_class:
        return "py_%s" % value.__name__
    elif cls == int:
        return "py_int"
    elif cls == float:
        return "py_float"
    elif cls == str:
        return "py_str"
    assert False, value

def prolog_module(name, python_module):
    code = ['']

    keys = module.keys()
    prolog_fields = ", ".join("V_%s" % k for k in keys)

    for idx, key in enumerate(keys):
        code.append("object_attr_index(object('%s', %s), '%s', %s)." % (
                name, prolog_fields, key, idx + 2))

    prolog_ctor_field = ", ".join(python_value_to_prolog(module[key]) for key in keys)

    code.append('')
    code.append("module_%s(object('%s', %s))." % (name, name, prolog_ctor_field))

    code.append('')
    for value in python_module.values():
        if value.__class__ == function_class:
            code.append(prolog_predicate_for_python_module(value))

    return "\n".join(code)

code = """
A = 1
B = 1.0
C = "c"

def plus_C(value):
    return C + value

def func0(a):
    global A, B, C
    A = "hello"
    result = plus_C(A)
    C = 10
    result = plus_C(B) + a
    return result

def func1(lla, lb):
    return lla[0] + lb
"""

module = {}
exec(code, module)
del module['__builtins__']

print(":- style_check(-singleton).")
print(prolog_module("Module", module))
