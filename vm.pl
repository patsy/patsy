?- consult('python_code.pl').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%

type(py_int).
type(py_float).
type(py_str).
type(py_list).
type(py_tuple).
type(py_dict).
type(py_set).

%% Instructions

binary_add(py_float, py_float, py_float).
binary_add(py_int, py_float, py_float).
binary_add(py_float, py_int, py_float).
binary_add(py_int, py_int, py_int).
binary_add(py_str, py_str, py_str).
binary_add(py_list(A), py_list(A), py_list(A)). % TODO: This is not correct.
binary_add(py_tuple, py_tuple, py_tuple).

binary_subtract(py_float, py_float, py_float).
binary_subtract(py_int, py_float, py_float).
binary_subtract(py_float, py_int, py_float).

binary_subscr(py_list(Z), py_int, Z).
binary_subscr(py_dict(K, V), K, V).
binary_subscr(py_tuple(Z), py_int, Z).
binary_subscr(py_str, py_int, py_str).

return_value(V, V).

load_attr(Object, AttrName, Result) :-
    object_attr_index(Object, AttrName, Index),
    arg(Index, Object, Result).

store_attr(Object, AttrName, Value) :-
    object_attr_index(Object, AttrName, Index),
    setarg(Index, Object, Value).

call_function(Module, Func, ArgList, Result) :-
    callable(Func),
    append([Module], ArgList, T0),
    append(T0, [Result], PredicateArgs),
    apply(Func, PredicateArgs).

call_function(_Module, function(Result, ArgList), ArgList, Result).



